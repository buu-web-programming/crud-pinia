import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string) => {
    message.value = msg;
    isShow.value = true;
  };
  const closeMessage = (msg: string, tout: number = 2000) => {
    message.value = "";
    isShow.value = false;
    timeout.value = tout;
  };
  return { isShow, message, showMessage, closeMessage, timeout };
});
